# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
# Dušan Kazik <prescott66@gmail.com>, 2016-2020
# Jan Ziak <0xe2.0x9a.0x9b@gmail.com>, 2022
# Robert Hartl <hartl.robert@gmail.com>, 2010
# Slavko <linux@slavino.sk>, 2015
# 7dcd6f74323fe8d9c477949ff8fcbb1c_c427b63 <3fcd202e3dfab15fda15b8e88e54d449_7173>, 2011
# 7dcd6f74323fe8d9c477949ff8fcbb1c_c427b63 <3fcd202e3dfab15fda15b8e88e54d449_7173>, 2011
msgid ""
msgstr ""
"Project-Id-Version: Xfce Apps\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-08-03 00:50+0200\n"
"PO-Revision-Date: 2013-07-03 18:39+0000\n"
"Last-Translator: Jan Ziak <0xe2.0x9a.0x9b@gmail.com>, 2022\n"
"Language-Team: Slovak (http://www.transifex.com/xfce/xfce-apps/language/sk/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: sk\n"
"Plural-Forms: nplurals=4; plural=(n % 1 == 0 && n == 1 ? 0 : n % 1 == 0 && n >= 2 && n <= 4 ? 1 : n % 1 != 0 ? 2: 3);\n"

#: ../common/xfce-notify-log.c:555
msgid "Do you really want to clear the notification log?"
msgstr "Skutočne chcete vymazať záznam s oznámeniami?"

#: ../common/xfce-notify-log.c:559
msgid "Clear notification log"
msgstr "Vymazať záznam s oznámeniami"

#: ../common/xfce-notify-log.c:562
msgid "Cancel"
msgstr "Zrušiť"

#: ../common/xfce-notify-log.c:564 ../xfce4-notifyd-config/main.c:1118
msgid "Clear"
msgstr "Vymazať"

#: ../common/xfce-notify-log.c:580 ../common/xfce-notify-log.c:588
msgid "include icon cache"
msgstr "<br>"

#: ../panel-plugin/notification-plugin.c:227
#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:12
#: ../xfce4-notifyd-config/xfce4-notifyd-config.desktop.in.h:1
msgid "Notifications"
msgstr "Oznámenia"

#: ../panel-plugin/notification-plugin-dialogs.c:52
#, c-format
msgid "Unable to open the following url: %s"
msgstr "Nie je možné otvoriť nasledovnú adresu URL: %s"

#. create the dialog
#: ../panel-plugin/notification-plugin-dialogs.c:77
msgid "Notification Plugin Settings"
msgstr "Nastavenia zásuvného modulu oznámení"

#: ../panel-plugin/notification-plugin-dialogs.c:80
#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:13
msgid "_Help"
msgstr "_Pomocník"

#: ../panel-plugin/notification-plugin-dialogs.c:81
#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:14
msgid "_Close"
msgstr "_Zavrieť"

#: ../panel-plugin/notification-plugin-dialogs.c:99
msgid "Number of notifications to show"
msgstr "Počet oznámení, ktoré sa majú zobraziť"

#: ../panel-plugin/notification-plugin-dialogs.c:111
msgid "Only show notifications from today"
msgstr "Zobraziť iba dnešné oznámenia"

#: ../panel-plugin/notification-plugin-dialogs.c:124
msgid "Hide 'Clear log' confirmation dialog"
msgstr "Skryť potvrdzujúce dialógové okno „Vymazať záznam“"

#: ../panel-plugin/notification-plugin-dialogs.c:161
msgid "This is the notification plugin"
msgstr "Toto je zásuvný modul oznámení"

#: ../panel-plugin/notification-plugin-dialogs.c:163
msgid "Copyright © 2017 Simon Steinbeiß\n"
msgstr "Autorské práva © 2017 Simon Steinbeiß\n"

#: ../panel-plugin/notification-plugin-log.c:140
msgid "<b>_Do not disturb</b>"
msgstr "<b>_Nevyrušovať</b>"

#: ../panel-plugin/notification-plugin-log.c:336
msgid "No notifications"
msgstr "Žiadne oznámenia"

#: ../panel-plugin/notification-plugin-log.c:357
msgid "_Clear log"
msgstr "Vy_mazať záznam"

#: ../panel-plugin/notification-plugin-log.c:365
msgid "_Notification settings…"
msgstr "_Nastavenia oznámení…"

#: ../panel-plugin/notification-plugin.desktop.in.h:1
msgid "Notification Plugin"
msgstr "Zásuvný modul oznámení"

#: ../panel-plugin/notification-plugin.desktop.in.h:2
msgid "Notification plugin for the Xfce panel"
msgstr "Zásuvný modul oznámení pre panel prostredia Xfce"

#: ../xfce4-notifyd/main.c:53 ../xfce4-notifyd/main.c:63
#: ../xfce4-notifyd-config/main.c:963
msgid "Xfce Notify Daemon"
msgstr "Démon oznámení v Xfce"

#: ../xfce4-notifyd/main.c:56
#, c-format
msgid "Unknown option \"%s\"\n"
msgstr "Neznáma voľba \"%s\"\n"

#: ../xfce4-notifyd/main.c:65
msgid "Unable to start notification daemon"
msgstr "Nemožno spustiť démona oznámení"

#: ../xfce4-notifyd/xfce-notify-daemon.c:405
#, c-format
msgid "Another notification daemon is running, exiting\n"
msgstr "Iný démon oznámení je už spustený. Ukončuje sa.\n"

#: ../xfce4-notifyd-config/main.c:86
msgid "Notification Preview"
msgstr "Ukážka oznámenia"

#: ../xfce4-notifyd-config/main.c:87
msgid "This is what notifications will look like"
msgstr "Takto bude vyzerať oznámenie"

#: ../xfce4-notifyd-config/main.c:92
msgid "Button"
msgstr "Tlačidlo"

#: ../xfce4-notifyd-config/main.c:99
msgid "Notification preview failed"
msgstr "Náhľad oznámenia zlyhal"

#: ../xfce4-notifyd-config/main.c:470
msgid "Unspecified applications"
msgstr "Nešpecifikované aplikácie"

#: ../xfce4-notifyd-config/main.c:651
#, c-format
msgid ""
"<b>Currently only urgent notifications are shown.</b>\n"
"Notification logging is %s."
msgstr "<b>Momentálne sú zobrazované iba dôležité oznámenia.</b>\nZaznamenávanie oznámení je %s."

#: ../xfce4-notifyd-config/main.c:658
msgid "enabled"
msgstr "povolené"

#: ../xfce4-notifyd-config/main.c:658
msgid "disabled"
msgstr "zakázané"

#: ../xfce4-notifyd-config/main.c:738
msgid "Yesterday and before"
msgstr "Včera a pred tým"

#: ../xfce4-notifyd-config/main.c:965
msgid "Settings daemon is unavailable"
msgstr "Nastavenia démona nie sú k dispozícii"

#: ../xfce4-notifyd-config/main.c:1051
msgid ""
"<big><b>Currently there are no known applications.</b></big>\n"
"As soon as an application sends a notification\n"
"it will appear in this list."
msgstr "<big><b>V súčasnosti nie sú známe žiadne aplikácie.</b></big>\nHneď ako aplikácia odošle upozornenie,\nzobrazí sa v tomto zozname."

#: ../xfce4-notifyd-config/main.c:1096
msgid ""
"<big><b>Empty log</b></big>\n"
"No notifications have been logged yet."
msgstr "<big><b>Prázdny záznam</b></big>\nZatiaľ neboli zaznamenané žiadne oznámenia."

#: ../xfce4-notifyd-config/main.c:1104
msgid "Refresh"
msgstr "Obnoviť"

#: ../xfce4-notifyd-config/main.c:1105
msgid "Refresh the notification log"
msgstr "Obnoviť záznam s oznámeniami"

#: ../xfce4-notifyd-config/main.c:1111
msgid "Open"
msgstr "Otvoriť"

#: ../xfce4-notifyd-config/main.c:1112
msgid "Open the notification log in an external editor"
msgstr "Otvoriť záznam s oznámeniami v externom editore"

#: ../xfce4-notifyd-config/main.c:1119
msgid "Clear the notification log"
msgstr "Vymazať záznam s oznámeniami"

#: ../xfce4-notifyd-config/main.c:1141
msgid "Display version information"
msgstr "Zobrazí informácie o verzii"

#: ../xfce4-notifyd-config/main.c:1142
msgid "Settings manager socket"
msgstr "Soket správcu nastavení"

#: ../xfce4-notifyd-config/main.c:1142
msgid "SOCKET_ID"
msgstr "ID SOKETU"

#: ../xfce4-notifyd-config/main.c:1152
#, c-format
msgid "Type '%s --help' for usage."
msgstr "Informácie o použití získate príkazom '%s --help'."

#: ../xfce4-notifyd-config/main.c:1167
#, c-format
msgid "Released under the terms of the GNU General Public License, version 2\n"
msgstr "Vydané pod GNU General Public License verzie 2\n"

#: ../xfce4-notifyd-config/main.c:1168
#, c-format
msgid "Please report bugs to %s.\n"
msgstr "Prosím, oznámte nájdené chyby na <%s>.\n"

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:1
msgid "only during \"Do not disturb\""
msgstr "iba počas režimu „Nevyrušovať“"

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:2
msgid "always"
msgstr "vždy"

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:3
msgid "all"
msgstr "všetky"

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:4
msgid "all except blocked"
msgstr "všetky okrem zablokovaných"

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:5
msgid "only blocked"
msgstr "iba zablokované"

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:6
msgid "display with mouse pointer"
msgstr "zobraziť pomocou ukazovateľa myši"

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:7
msgid "primary display"
msgstr "hlavný displej"

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:8
msgid "Top left"
msgstr "Vľavo hore"

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:9
msgid "Bottom left"
msgstr "Vľavo dole"

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:10
msgid "Top right"
msgstr "Vpravo hore"

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:11
msgid "Bottom right"
msgstr "Vpravo dole"

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:15
msgid ""
"The notification service is not running. No notifications will be shown."
msgstr "Služba oznámení nie je spustená. Nebudú zobrazené žiadne oznámenia."

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:16
msgid "<b>Currently only urgent notifications are shown.</b>"
msgstr "<b>Momentálne sa zobrazujú iba dôležité oznámenia.</b>"

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:17
msgid "Show _Preview"
msgstr "Zobraziť _náhľad"

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:18
msgid "seconds"
msgstr "sekundách"

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:19
msgid "_Disappear after"
msgstr "Vymazať _po dobe"

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:20
msgid "_Opacity"
msgstr "_Krytie"

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:21
msgid "Fade out"
msgstr "Zoslabiť"

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:22
msgid "_Slide out"
msgstr "Vy_sunúť"

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:23
msgid "Default _position"
msgstr "Predvolená _pozícia"

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:24
msgid "_Theme"
msgstr "_Téma"

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:25
msgid "Do not disturb"
msgstr "Nevyrušovať"

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:26
msgid "<b>Behavior</b>"
msgstr "<b>Správanie</b>"

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:27
msgid "<b>Appearance</b>"
msgstr "<b>Vzhľad</b>"

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:28
msgid "<b>Animations</b>"
msgstr "<b>Animácie</b>"

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:29
msgid ""
"By default the notification bubbles will be shown on the display on which "
"the mouse pointer is located."
msgstr "Predvolene sa budú zobrazovať bubliny oznámení na tom displeji, na ktorom bude umiestnený ukazovateľ myši."

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:30
msgid "Show notifications on"
msgstr "Zobraziť oznámenia na"

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:31
msgid "General"
msgstr "Všeobecné"

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:32
msgid "<b>Show or block notifications per application</b>"
msgstr "<b>Zobraziť alebo zablokovať oznámenia podľa aplikácie</b>"

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:33
msgid "Applications"
msgstr "Aplikácie"

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:34
msgid "Log notifications"
msgstr "Zaznamenávať aplikácie"

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:35
msgid "Log applications"
msgstr "Zaznamenávať aplikácie"

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:36
msgid "Log size limit"
msgstr "Limit počtu záznamov"

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:37
msgid ""
"The maximum number of entries to be retained in the log. Please note that a "
"large log may lead to performance penalties. 0 means no limit."
msgstr "Maximálny počet záznamov, ktoré sa majú zachovať v denníku. Upozornenie: Veľký denník môže mať nepriaznivý vplyv na výkon. 0, nula, znamená, že počet záznamov je bez obmedzenia."

#: ../xfce4-notifyd-config/xfce4-notifyd-config.glade.h:38
msgid "Log"
msgstr "Zaznamenávanie"

#: ../xfce4-notifyd-config/xfce4-notifyd-config.desktop.in.h:2
msgid "Customize how notifications appear on your screen"
msgstr "Prispôsobuje vzhľad oznámení na vašej obrazovke"
